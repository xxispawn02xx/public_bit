#!/usr/bin/env python
# coding: utf-8

# In[1]:


# Import Libraries
from os import listdir
from csv import writer
from csv import reader
import datetime
import pandas as pd
import numpy as np


# In[3]:


# Read csv file
df = pd.read_csv("C:/Users/PoojaLocal/Desktop/ZillowAnalysis.csv")
df.head(5)


# In[4]:


#create a list of all the columns
columns = list(df)
#create lists to hold headers & months
headers = []
rows = []
#split columns list into headers and months
for col in columns:
    if col.startswith('M,'):
        rows.append(col)
    else:
        headers.append(col)


# In[5]:


# Combine columns to rows
df2 = pd.melt(df,
                  id_vars=headers,
                  value_vars=rows,
                  var_name='Year',
                  value_name='Home Values')
df2.tail(5)


# In[9]:


# Separate by ,
i = df2.columns.get_loc('Year')
df3 = df2['Year'].str.split(",", expand=True)
df4 = pd.concat([df2.iloc[:, :i], df3, df2.iloc[:, i+1:]], axis=1)
df4.head(5)


# In[10]:


# Drop a column by Column Number
df5 = df4.drop(df4.columns[9], axis=1)
df5.head(5)


# In[13]:


# Rename a column by Column Number
df5.columns.values[9] = "Timeline"
df5.head(5)


# In[14]:


# Save the file on computer
df5.to_csv('ZillowTimeline.csv')


# In[ ]:




